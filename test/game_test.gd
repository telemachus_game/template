# GdUnit generated TestSuite
class_name GameTest
extends GdUnitTestSuite
@warning_ignore('unused_parameter')
@warning_ignore('return_value_discarded')

# TestSuite generated from
const __source = 'res://game.gd'


func test_returns_5() -> void:
	var game_object = auto_free(load("res://game.gd").new())
	# remove this line and complete your test
	Print.info("A Unit test is running!")
	assert_int(game_object.returns_5()).is_equal(5)
