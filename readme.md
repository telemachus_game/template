# Godot 4 Game Template
This game is an example project for Godot 4. This template has CI-CD deployment, unit tests, and logging functionality.
### IMPORTANT! - This project uses submodules. Use the following commands to run the code.
1. `git clone <project.git>` - Replace project.git with the URL of this repository.
2. `cd project` - Enter the project folder.
3. `git submodule update --init`

## This project is an empty game template with multiple addons.
The template project shows the most basic use for each module below.

### GdUnit4
Used to run unit tests against your code. There is a simple example test provided in the template. For more information, visit the [GdUnit4 Docs](https://mikeschulze.github.io/gdUnit4/first_steps/firstTest/).

### Sdg-Print logger
Allows you to print at different levels, or create module-specific loggers. Loggers can print everything to the console, or can be configured to store all prints and only output them in the case of an error.

See the example script for basic usage.

### Godot Console
Allows you to run simple commands from a drop-down console window. Press the \` key (~ key) to open a drop-down console in the game. The console will display prints from the logging module, and allows you to run developer functions and commands in-game. Click [here](https://github.com/quentincaffeino/godot-console) for more information.

### FPS Counter
Displays the current game FPS. Overlay can be toggled by typing "FPS" in the console window.
